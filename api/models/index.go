package models

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/spf13/viper"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB
var err error

// Models struct
type Models struct {
	*sql.DB
}

// Init creates a connection to mysql database and
// migrates any new models
func Init() {
	var connectionURL string

	connectionURL = fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&collation=utf8mb4_unicode_ci&parseTime=true&loc=Local", viper.GetString("database.DBUsername"), viper.GetString("database.DBPassword"), viper.GetString("database.DBHostname"), viper.GetString("database.Port"), viper.GetString("database.DBName"))

	db, _ = gorm.Open("mysql", connectionURL)

	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)

	db.LogMode(true)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Database connected successfully")
	}
}

// GetDB ...
func GetDB() *gorm.DB {
	return db
}

// CloseDB connection
func CloseDB() {
	db.Close()
}
