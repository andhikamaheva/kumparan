package models

// Response ...
type Response struct {
	Type string                 `json:"type"`
	Code int                    `json:"code"`
	Data map[string]interface{} `json:"data"`
}
