package models

import (
	"time"
)

// News ...
type News struct {
	ID      int       `json:"id"`
	Author  string    `json:"author,omitempty" validate:"required"`
	Body    string    `json:"body,omitempty" validate:"required"`
	Created time.Time `json:"created,omitempty"`
}
