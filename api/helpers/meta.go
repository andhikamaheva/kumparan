package helpers

// Meta ...
type Meta struct {
	Type     string    `json:"type"`
	Code     int       `json:"code"`
	Messages []Message `json:"messages"`
}

// Message ...
type Message struct {
	Param   string `json:"param"`
	Message string `json:"message"`
	Value   string `json:"value"`
}

// Response ...
func (m *Meta) Response(responseType string, code int, messages []Message) Meta {
	var meta Meta

	if code == 200 {
		meta = Meta{Type: responseType, Code: code, Messages: messages}
	} else {
		meta = Meta{Type: responseType, Code: code, Messages: messages}
	}

	return meta
}
