package helpers

import (
	"reflect"
	"strconv"
	"strings"

	en_translations "kumparan.com/api/translations/validator/en"
	id_translations "kumparan.com/api/translations/validator/id"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/id"
	ut "github.com/go-playground/universal-translator"
	validator "gopkg.in/go-playground/validator.v9"
)

// Validator ...
type Validator struct{}

// ValidateErrors ...
func (v *Validator) ValidateErrors(c *gin.Context, valid *validator.Validate, errors error) []Message {
	en := en.New()
	id := id.New()
	var messages []Message

	uni := ut.New(en, en, id)

	trans, result := uni.GetTranslator("en")

	if len(c.Request.Header["Accept-Language"]) > 0 {
		trans, result = uni.FindTranslator(c.Request.Header["Accept-Language"][0])
	}

	if result {
		if trans.Locale() == "en" {
			en_translations.RegisterDefaultTranslations(valid, trans)
		} else if trans.Locale() == "id" {
			id_translations.RegisterDefaultTranslations(valid, trans)
		} else {
			en_translations.RegisterDefaultTranslations(valid, trans)
		}

		// var value string
		for _, err := range errors.(validator.ValidationErrors) {
			var value string

			switch reflect.ValueOf(err.Value()).Kind() {

			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				if err.Value().(int64) == 0 {
					value = ""
				} else {
					value = strconv.Itoa(err.Value().(int))
				}
				/* 	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
				value = strconv.FormatUint((err.Value().(uint)) */

			case reflect.Float32, reflect.Float64:
				value = strconv.FormatFloat(err.Value().(float64), 'f', 1, 64)

			case reflect.String:
				value = err.Value().(string)
			}
			messages = append(messages, Message{Param: strings.ToLower(err.Field()), Message: err.Translate(trans), Value: value})
		}
	} else {
		messages = append(messages, Message{Param: "", Message: "", Value: ""})
	}

	return messages

}

// Init ...
func (v *Validator) Init() *validator.Validate {
	validator := validator.New()

	validator.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	return validator

}
