package helpers

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/id"
	ut "github.com/go-playground/universal-translator"
)

var (
	utrans *ut.UniversalTranslator
)

// Translator ...
type Translator struct{}

// Translate ...
func (t *Translator) Translate(c *gin.Context) ut.Translator {
	en := en.New()
	id := id.New()

	uni := ut.New(en, en, id)

	err := uni.Import(ut.FormatJSON, "translations")
	if err != nil {
		log.Fatal(err)
	}

	err = uni.VerifyTranslations()
	if err != nil {
		log.Fatal(err)
	}

	trans, result := uni.GetTranslator("en")

	if len(c.Request.Header["Accept-Language"]) > 0 {
		trans, result = uni.FindTranslator(c.Request.Header["Accept-Language"][0])
	}

	if result {
		if trans.Locale() == "en" || trans.Locale() == "id" {
			return trans
		} else {
			trans, _ := uni.GetTranslator("en")
			return trans
		}

	}
	return trans
}
