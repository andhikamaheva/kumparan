package helpers

import (
	"context"
	"fmt"

	"gopkg.in/olivere/elastic.v5"
)

// ElasticSearch ...
type ElasticSearch struct{}

// GetClient ...
func (e *ElasticSearch) GetClient() *elastic.Client {
	client, err := elastic.NewSimpleClient(
		elastic.SetURL("http://elasticsearch:9200"),
		elastic.SetSniff(false),
	)
	if err != nil {
		failOnError(err, "Failed connect to ElasticSearch server")
	} else {
		info, code, err := client.Ping("http://elasticsearch:9200").Do(context.Background())
		if err != nil {
			// Handle error
			panic(err)
		}
		fmt.Printf("Elasticsearch returned with code %d and version %s\n", code, info.Version.Number)

		return client
	}
	return client
}

func failOnError(err error, msg string) {
	if err != nil {
		fmt.Printf("%s: %s \n", msg, err)
	}
}
