package helpers

import (
	"time"

	"github.com/robfig/go-cache"
)

var cacheClient *cache.Cache

// Cache ...
type Cache struct{}

// Init ...
func (c *Cache) Init() {
	ch := cache.New(5*time.Minute, 10*time.Minute)
	cacheClient = ch
}

// GetClient ...
func (c *Cache) GetClient() *cache.Cache {
	return cacheClient
}
