package v1

import (
	"time"

	"github.com/gin-contrib/cache"
	"github.com/gin-contrib/cache/persistence"
	controllers "kumparan.com/api/controllers/v1"

	"github.com/gin-gonic/gin"
)

// Routes ...
func Routes(route *gin.Engine) {
	NewsController := new(controllers.NewsController)

	// Init Cache Middleware (In Memory Caching)
	store := persistence.NewInMemoryStore(time.Minute)

	route.POST("/news", NewsController.Store)
	route.GET("/news", cache.CachePage(store, time.Minute, NewsController.Index))

}
