package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func performRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestFetchAllNews(t *testing.T) {
	router := SetupRouter()

	req, _ := http.NewRequest("GET", "/news", nil)
	res := httptest.NewRecorder()
	router.ServeHTTP(res, req)

	var response map[string]interface{}
	err := json.Unmarshal([]byte(res.Body.String()), &response)

	assert.Equal(t, http.StatusOK, res.Code)                                      // Check status code 200
	assert.Nil(t, err)                                                            // Check no error on response
	assert.Equal(t, "success", response["meta"].(map[string]interface{})["type"]) // Check meta type = success
}

func TestCreateNews(t *testing.T) {
	router := SetupRouter()

	var body = []byte(`{
		"author": "Andhika Maheva",
		"body": "Laporan Hasil Pemilu 2019"
		}`)

	req, _ := http.NewRequest("POST", "/news", bytes.NewBuffer(body))
	res := httptest.NewRecorder()
	router.ServeHTTP(res, req)

	var response map[string]interface{}
	err := json.Unmarshal([]byte(res.Body.String()), &response)

	assert.Equal(t, http.StatusCreated, res.Code)                                             // Check status code 201
	assert.Nil(t, err)                                                                        // Check no error on response
	assert.Equal(t, "success_create_news", response["meta"].(map[string]interface{})["type"]) // Check meta type = success_create_news
}
