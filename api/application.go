package main

import (
	"log"
	"time"

	"kumparan.com/api/models"
	v1 "kumparan.com/api/routers/v1"
	"kumparan.com/api/services"

	"kumparan.com/api/config"

	helmet "github.com/danielkov/gin-helmet"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// SetupRouter ...
func SetupRouter() *gin.Engine {
	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{
			"http://127.0.0.1",
		},
		AllowMethods: []string{
			"PUT",
			"GET",
			"POST",
			"DELETE",
			"OPTIONS",
			"PATCH",
		},
		AllowHeaders: []string{"Access-Control-Allow-Origin",
			"Origin",
			"X-Requested-With",
			"Content-Type",
			"Accept",
			"Authorization",
			"Access-Control-Allow-Credentials",
			"Access-Control-Allow-Headers",
			"Accept-Language",
		},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	r.Use(helmet.Default())

	r.Use(gin.Logger())

	viper.SetConfigName("config")
	viper.AutomaticEnv()
	viper.AddConfigPath(".")
	var configuration config.Configuration
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %v", err)
	}

	err := viper.Unmarshal(&configuration)
	if err != nil {
		log.Fatalf("Unable to decode into Struct, %v", err)
	}

	// go rabbitMQ.Consume("create_news")

	gin.SetMode(gin.ReleaseMode)

	rabbitMQ := new(services.RabbitMQ)
	elasticSearch := new(services.ElasticSearch)

	elasticSearch.Init()
	rabbitMQ.Init()
	models.Init()

	v1.Routes(r)

	// Diasallow all search engine indexing
	r.GET("/robots.txt", func(c *gin.Context) {
		c.String(200, "User-agent: * \nDisallow: /")
	})

	r.Use(gin.Recovery())

	return r

}

func main() {
	r := SetupRouter()
	r.Run(":5000")
}
