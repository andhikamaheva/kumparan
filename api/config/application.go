package config

// ApplicationConfiguration ...
type ApplicationConfiguration struct {
	AppURL          string
	Environment     string
	TimeZone        string
	LimitPagination int
}
