package config

type DatabaseConfiguration struct {
	DBUsername string
	DBPassword string
	DBHostname string
	DBName     string
	Port       int
}
