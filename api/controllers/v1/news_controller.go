package v1

import (
	"context"
	"encoding/json"
	"runtime"
	"strconv"

	"kumparan.com/api/helpers"
	"kumparan.com/api/models"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/olivere/elastic"
	"github.com/spf13/viper"
)

// NewsController ...
type NewsController struct{}

// Index ...
func (n *NewsController) Index(c *gin.Context) {
	var news []models.News

	message, _ := T.Translate(c).T("success_showing_data")

	var limit int

	var page int

	queryLimit, _ := strconv.Atoi(c.Query("limit"))

	if queryLimit == 0 {
		limit = viper.GetInt("application.LimitPagination")
	} else {
		limit = queryLimit
	}

	queryPage, _ := strconv.Atoi(c.Query("page"))

	if queryPage == 0 {
		page = 1
	} else {
		page = queryPage
	}

	searchResult, err := ElasticSearch.GetClient().Search().
		Index("news").
		Type("news").
		Query(elastic.MatchAllQuery{}).
		Sort("created", false).
		From((page - 1) * limit).
		Size(limit).
		Pretty(true).
		Do(context.Background())

	if err != nil || len(searchResult.Hits.Hits) < 1 {
		c.JSON(200, gin.H{
			"data":       nil,
			"pagination": Pagination.Make(0, 0, 0, 0, c.Request.URL.Query(), c.Request.URL.String()),
			"meta":       Meta.Response("success", 200, []helpers.Message{{Param: "", Message: message, Value: ""}}),
		})
	} else {

		runtime.GOMAXPROCS(2)
		var newsChannel = make(chan models.News)

		for _, result := range searchResult.Hits.Hits {
			go func(newsChannel chan models.News, id string) {
				var news models.News
				if err := models.GetDB().Where("id = ? ", id).Find(&news); err.Error == nil {
					newsChannel <- news
				}
			}(newsChannel, result.Id)
			var data = <-newsChannel
			news = append(news, data)
		}

		var total int

		if len(searchResult.Hits.Hits) == 0 || len(news) == 0 {
			total = 0
		} else if len(searchResult.Hits.Hits) != len(news) {
			// Normalize data total when real data doesn't exists
			total = (int(searchResult.TotalHits()) - (len(searchResult.Hits.Hits) - len(news)))
		} else {
			total = int(searchResult.TotalHits())
		}

		c.JSON(200, gin.H{
			"data":       news,
			"pagination": Pagination.Make(total, len(news), limit, page, c.Request.URL.Query(), c.Request.URL.String()),
			"meta":       Meta.Response("success", 200, []helpers.Message{{Param: "", Message: message, Value: ""}}),
		})
	}

}

// Store ...
func (n *NewsController) Store(c *gin.Context) {
	var validator = new(helpers.Validator)
	var validation = validator.Init()

	type requestJSON struct {
		Author string `json:"author" validate:"required"`
		Body   string `json:"body" validate:"required"`
	}

	var request requestJSON
	if err := c.ShouldBindBodyWith(&request, binding.JSON); err != nil {
		message, _ := T.Translate(c).T("failed_create_news")
		c.JSON(400, gin.H{
			"meta": Meta.Response("failed_create_news", 400, []helpers.Message{{Param: "", Message: message, Value: ""}}),
		})
	} else {
		if errors := validation.Struct(request); errors != nil {
			messages := validator.ValidateErrors(c, validation, errors)
			c.JSON(400, gin.H{
				"meta": Meta.Response("failed_create_news", 400, messages),
			})
		} else {

			body, _ := json.Marshal(request)
			response := RabbitMQ.Produce("news", "news.create", body)
			message, _ := T.Translate(c).T(response.Type)

			c.JSON(response.Code, gin.H{
				"meta": Meta.Response(response.Type, response.Code, []helpers.Message{{Param: "", Message: message, Value: ""}}),
			})
		}
	}
}

// Show ...
func (n *NewsController) Show(c *gin.Context) {

}

// Update ...
func (n *NewsController) Update(c *gin.Context) {

}

// Destroy ...
func (n *NewsController) Destroy(c *gin.Context) {

}
