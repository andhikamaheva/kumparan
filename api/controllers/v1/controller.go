package v1

import (
	"kumparan.com/api/helpers"
	"kumparan.com/api/services"

	ut "github.com/go-playground/universal-translator"
)

// Meta ...
var Meta = new(helpers.Meta)

// Pagination ...
var Pagination = new(helpers.Pagination)

// T ...
var T = new(helpers.Translator)

// RabbitMQ ...
var RabbitMQ = new(services.RabbitMQ)

// ElasticSearch ...
var ElasticSearch = new(helpers.ElasticSearch)

var (
	uni *ut.UniversalTranslator
)
