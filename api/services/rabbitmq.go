package services

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/streadway/amqp"
	"kumparan.com/api/models"
)

// RabbitMQ ...
type RabbitMQ struct{}

var connection *amqp.Connection
var channel *amqp.Channel

// Init ...
func (m *RabbitMQ) Init() {
	conn, err := amqp.Dial("amqp://rabbitmq:rabbitmq@rabbitmq:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	connection = conn

	ch, err := conn.Channel()
	channel = ch
	failOnError(err, "Failed to open a channel")

}

// Produce ...
func (m *RabbitMQ) Produce(exchangeName string, routingKey string, body []byte) models.Response {
	err := channel.ExchangeDeclare(
		exchangeName,
		"topic",
		false,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare a exchange")

	q, err := channel.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)

	failOnError(err, "Failed to declare a queue")

	corrID := time.Now().Unix()

	msgs, err := channel.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	failOnError(err, "Failed to register a consumer")

	err = channel.Publish(
		exchangeName,
		routingKey,
		false,
		false,
		amqp.Publishing{
			ContentType:   "application/json",
			CorrelationId: string(corrID),
			ReplyTo:       q.Name,
			Body:          body,
		})
	failOnError(err, "Failed to publish a message")

	for msg := range msgs {

		if string(corrID) == msg.CorrelationId {
			var response models.Response
			json.Unmarshal(msg.Body, &response)
			msg.Ack(false)
			return response
		}
	}

	return models.Response{}

}

// Consume ...
func (m *RabbitMQ) Consume(exchangeName string, routingKey string) {
	err := channel.ExchangeDeclare(
		exchangeName,
		"topic",
		false,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare a exchange")

	q, err := channel.QueueDeclare(
		exchangeName, // name
		false,        // durable
		false,        // delete when usused
		false,        // exclusive
		false,        // no-wait
		nil,          // arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = channel.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	failOnError(err, "Failed to set QoS")

	err = channel.QueueBind(
		exchangeName, // queue name
		routingKey,   // binding key
		exchangeName, // exchange
		false,
		nil,
	)

	failOnError(err, "Failed to binding the queue")

	msgs, err := channel.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for msg := range msgs {
			switch msg.RoutingKey {

			}
		}
	}()

	log.Printf(" [*] Waiting for messages Exchange = %s and RoutingKey = %s . To exit press CTRL+C \n", q.Name, routingKey)

	<-forever
}

// CloseConnection ...
func (m *RabbitMQ) CloseConnection() {
	defer channel.Close()
}

func failOnError(err error, msg string) {
	if err != nil {
		fmt.Printf("%s: %s \n", msg, err)
	}
}
