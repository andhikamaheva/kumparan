package config

// Configuration ...
type Configuration struct {
	// Server      ServerConfiguration
	Application ApplicationConfiguration
	Database    DatabaseConfiguration
	// Cloudinary  CloudinaryConfiguration
}
