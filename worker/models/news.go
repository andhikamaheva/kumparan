package models

import (
	"time"
)

// News ...
type News struct {
	ID      int       `json:"id"`
	Author  string    `json:"author,omitempty"`
	Body    string    `json:"body,omitempty"`
	Created time.Time `json:"created,omitempty"`
}
