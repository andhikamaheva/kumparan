package main

import (
	"log"
	"time"

	"kumparan.com/worker/config"
	"kumparan.com/worker/models"
	"kumparan.com/worker/services"

	helmet "github.com/danielkov/gin-helmet"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{
			"http://127.0.0.1",
		},
		AllowMethods: []string{
			"PUT",
			"GET",
			"POST",
			"DELETE",
			"OPTIONS",
			"PATCH",
		},
		AllowHeaders: []string{"Access-Control-Allow-Origin",
			"Origin",
			"X-Requested-With",
			"Content-Type",
			"Accept",
			"Authorization",
			"Access-Control-Allow-Credentials",
			"Access-Control-Allow-Headers",
			"Accept-Language",
		},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	r.Use(helmet.Default())

	r.Use(gin.Logger())

	viper.SetConfigName("config")
	viper.AutomaticEnv()
	viper.AddConfigPath(".")
	var configuration config.Configuration
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %v", err)
	}

	err := viper.Unmarshal(&configuration)
	if err != nil {
		log.Fatalf("Unable to decode into Struct, %v", err)
	}

	rabbitMQ := new(services.RabbitMQ)
	elasticSearch := new(services.ElasticSearch)

	models.Init()
	rabbitMQ.Init()
	elasticSearch.Init()

	rabbitMQ.Consume("news", "news.create")

	gin.SetMode(gin.ReleaseMode)

	// Diasallow all search engine indexing
	r.GET("/robots.txt", func(c *gin.Context) {
		c.String(200, "User-agent: * \nDisallow: /")
	})

	r.Use(gin.Recovery())

	return r

}

func main() {
	r := setupRouter()
	r.Run(":5001")
}
