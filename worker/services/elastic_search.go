package services

import (
	"context"
	"fmt"

	"gopkg.in/olivere/elastic.v5"
)

// ElasticSearch ...
type ElasticSearch struct{}

// Client ...
var client *elastic.Client

// Init ...
func (e *ElasticSearch) Init() {
	c, err := elastic.NewClient(elastic.SetURL("http://elasticsearch:9200"), elastic.SetSniff(false))
	if err != nil {
		failOnError(err, "Failed connect to ElasticSearch server")
		panic(err)
	} else {
		fmt.Println("Connected to ElasticSearch server")
		client = c

		const newsMapping = `
		{
			"settings":{
				"number_of_shards": 1,
				"number_of_replicas": 0
			},
			"mappings":{
				"news":{
						"properties":{
							"created":{
								"type":"date"
							}
						}
					}
			}
		}`

		e.CreateIndex("news", newsMapping)
	}
}

// CreateIndex ...
func (e *ElasticSearch) CreateIndex(name string, mapping string) {
	isIndexExist, err := client.IndexExists("news").Do(context.Background())
	failOnError(err, "Failed checking index already exist or no")
	if !isIndexExist {
		createdIndex, err := client.CreateIndex("news").BodyString(mapping).Do(context.Background())

		failOnError(err, "Failed to create index")
		if !createdIndex.Acknowledged {
			fmt.Println("Index is not acnkowledged")
		}
	}
	if isIndexExist {
		fmt.Printf("%s Index already exist\n", name)
	}
}

// GetClient ...
func (e *ElasticSearch) GetClient() *elastic.Client {
	return client
}
