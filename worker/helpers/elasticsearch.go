package helpers

import (
	"fmt"

	"gopkg.in/olivere/elastic.v5"
)

// ElasticSearch ...
type ElasticSearch struct{}

// GetClient ...
func (e *ElasticSearch) GetClient() *elastic.Client {
	client, err := elastic.NewClient(elastic.SetURL("http://elasticsearch:9200"), elastic.SetSniff(false))
	if err != nil {
		failOnError(err, "Failed connect to ElasticSearch server")
	} else {
		return client
	}
	return client
}

func failOnError(err error, msg string) {
	if err != nil {
		fmt.Printf("%s: %s \n", msg, err)
	}
}
