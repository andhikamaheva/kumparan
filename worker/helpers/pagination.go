package helpers

import (
	"math"
	"regexp"
	"strconv"
	"strings"

	"github.com/spf13/viper"
)

// Pagination ...
type Pagination struct {
	Total       int    `json:"total"`
	PerPage     int    `json:"per_page"`
	CurrentPage int    `json:"current_page"`
	LastPage    int    `json:"last_page"`
	NextPageURL string `json:"next_page_url"`
	PrevPageURL string `json:"prev_page_url"`
	From        int    `json:"from"`
	To          int    `json:"to"`
}

// Make ...
func (p *Pagination) Make(total int, totalRows int, limit int, page int, query map[string][]string, originalURL string) Pagination {
	var nextPage = page + 1
	var prevPage = page - 1
	var nextURL string
	var lastPage float64
	var nextPageURL string
	var prevPageURL string
	var from int
	var to int
	var replacer = regexp.MustCompile(`(page)[^\\&?]+`)

	if query == nil {
		nextURL = viper.GetString("application.AppURL") + originalURL + "?page=" + strconv.Itoa(nextPage)
	} else if strings.Contains(originalURL, "page=") == false {
		nextURL = viper.GetString("application.AppURL") + originalURL + "&page=" + strconv.Itoa(nextPage)
	} else {
		nextURL = viper.GetString("application.AppURL") + replacer.ReplaceAllString(originalURL, `$1=`+strconv.Itoa(nextPage))
	}

	if math.IsNaN(math.Ceil(float64(total) / float64(limit))) {
		lastPage = 0
	} else {
		lastPage = math.Ceil(float64(total) / float64(limit))
	}

	if math.Ceil(float64(total)/float64(limit)) == float64(page) || float64(total) == 0 {
		nextPageURL = ""
	} else {
		nextPageURL = nextURL
	}

	if (page-1) == 0 || (page-1) < 0 {
		prevPageURL = ""
	} else {
		prevPageURL = viper.GetString("application.AppURL") + replacer.ReplaceAllString(originalURL, `$1=`+strconv.Itoa(prevPage))
	}

	from = ((page - 1) * limit) + 1

	if totalRows != 0 {
		to = ((page - 1) * limit) + totalRows
	} else {
		to = total
	}

	return Pagination{Total: total, PerPage: limit, CurrentPage: page, LastPage: int(lastPage), NextPageURL: nextPageURL, PrevPageURL: prevPageURL, From: from, To: to}
}
