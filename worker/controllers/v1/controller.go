package v1

import (
	"kumparan.com/worker/helpers"

	ut "github.com/go-playground/universal-translator"
)

// Meta ...
var Meta = new(helpers.Meta)

// Pagination ...
var Pagination = new(helpers.Pagination)

// T ...
var T = new(helpers.Translator)

// elasticSearch ...
var elasticSearch = new(helpers.ElasticSearch)

var (
	uni *ut.UniversalTranslator
)
