package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"kumparan.com/worker/models"
)

// NewsController ...
type NewsController struct{}

// Index ...
func (n *NewsController) Index(body []byte) {

}

// Store ...
func (n *NewsController) Store(data []byte) models.Response {
	var news models.News
	if err := json.Unmarshal(data, &news); err != nil {
		return models.Response{
			Type: "failed_create_news",
			Code: 400,
			Data: nil,
		}
	} else {
		loc, _ := time.LoadLocation("Asia/Jakarta")
		createdAt := time.Now().In(loc)

		// Begin insert transaction
		tx := models.GetDB().Begin()
		news.Created = createdAt
		if err := tx.Create(&news); err.Error != nil {
			tx.Rollback()
			return models.Response{
				Type: "failed_create_news",
				Code: 400,
				Data: nil,
			}
		} else {
			_, err := elasticSearch.GetClient().Index().
				Index("news").
				Type("news").
				Id(strconv.Itoa(news.ID)).
				BodyJson(struct {
					Created time.Time `json:"created"`
				}{
					createdAt,
				}).
				Do(context.Background())

			if err != nil {
				fmt.Println(err)
				tx.Rollback()
				return models.Response{
					Type: "failed_create_news",
					Code: 400,
					Data: nil,
				}
			} else {
				tx.Commit()
				return models.Response{
					Type: "success_create_news",
					Code: 201,
					Data: nil,
				}
			}
		}
	}

}

// Show ...
func (n *NewsController) Show(bodu []byte) {

}

// Update ...
func (n *NewsController) Update(bodu []byte) {

}

// Destroy ...
func (n *NewsController) Destroy(bodu []byte) {

}
