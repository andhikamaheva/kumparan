FROM golang:alpine

RUN apk update && apk add --no-cache git

WORKDIR $GOPATH/src/message-queue

COPY . .

RUN go get -d -v

RUN go build -o api .

EXPOSE 5000:5000

CMD ["./api"]




