# kumparan

Pada repository ini terdapat aplikasi kumparan backend yang dikembangkan menggunakan bahasa pemrograman Golang untuk keperluan Backend Skill Test kumparan.com. Untuk dapat menjalankannya pastikan Anda telah **memiliki / menginstall Docker serta Postman** di komputer Anda.
Dalam aplikasi ini terdapat beberapa service yang saya gunakan, antara lain :

1. Nginx (Port 80)
2. MySQL (Port 3306)
3. RabbitMQ (Port 5672)
4. ElasticSearch
    - Port 9200
    - Port 9300
5. Kumparan API (Port 5000)
6. Kumparan API Worker (Port 5001)

Untuk menjalankannya Anda dapat melakukan beberapa tahapan sebagai berikut :

1. Clone / Download repository ini
2. Extract di Directory komputer Anda
3. Buka Terminal dan buka Directory untuk project aplikasi ini
4. Jalankan perintah `docker-compose up --build`
5. Tunggu sampai proses berjalan tanpa error / error berhasil teratasi (saya mengaktifkan mode always restart dimana akan menjadikan service yang error tersebut melakukan restart hingga masalah fix). Jika tiba-tiba terjadi error / docker-compose berhenti berjalan, Anda dapat menjalankan perintah `docker-compose up --build` kembali

Hal-hal yang mungkin terjadi saat proses `docker-compose up` :

1. Proses `up` mungkin membutuhkan waktu cukup lama karena dalam 1 (satu) `docker-compose` tersebut terdapat beberapa services yang dijalankan secara bersamaan
2. Jika proses `up` telah selesai dan Anda mencoba mengakses `localhost` kemudian menemukan error **502**, Anda dapat mematikan `docker-compose` dan coba menjalankannya kembali atau Anda dapat mencoba melakukan restart pada container **kumparan.com_nginx**

## Penggunaan Aplikasi
1. Untuk dapat mengunakan service kumparan tersebut, Anda dapat mendownload Postman Collection melalui [Link Ini](https://www.getpostman.com/collections/b0d76a675d1a6820c76a). Selain itu, saya juga telah menyertakan contoh request dan response pada Postman Example
2. Setelah proses selesai, Anda dapat mengimport Postman Collection tersebut ke dalam Postman Anda


## Live Version
Selain dapat dijalankan dengan `docker-compose`, Anda juga dapat mengakses aplikasi ini melalui alamat [http://35.198.194.17](http://35.198.194.17) yang telah saya deploy ke Google Cloud Platform.


## Endpoints
1. `GET /news` : untuk menampilkan semua **news** yang jumlah data dan pagination dapat disesuaikan mengikuti Postman Collection. (Untuk data default yang tampil adalah 10 data)
2. `POST /news` : untuk menambahkan **news** baru

## Unit Test
Untuk menjalankan `test`, Anda dapat mengakses folder **kumparan.com > api > application_test.go**

## Catatan
Untuk sistem caching saat ini saya hanya menggunakan in-memory caching, dimana data yang ditampilkan pada endpoint `GET /news` akan disimpan pada memory server. Sehingga jika nantinya terdapat load balancing dapat mengakibatkan cachenya tidak sinkron. Selain itu ketika terdapat penambahan data baru tidak ada penghapusan cache sehingga harus menunggu sekitar 1 menit hingga cache tersebut expired dan dapat menampilkan data terbaru


Jika Anda mengalami kendala dalam menggunakan aplikasi ini, Anda dapat menghubungi saya melalui :

- Email [andhikamaheva@gmail.com](mailto:andhikamaheva@gmail.com)
- Telegram [@andhikamaheva](https://t.me/andhikamaheva)